# -*- coding: utf-8 -*-
import re
import urllib.request
import requests
import json
from flask import Flask, request, jsonify
from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *

from config import *

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    # if not "music" in text:
    #     return "뮤직?"

    # # 여기에 함수를 구현해봅시다.
    # url = "https://music.bugs.co.kr/"
    # sourcecode = urllib.request.urlopen(url).read()
    # soup = BeautifulSoup(sourcecode, "html.parser")
    # musicList = []
    # for i in soup.find_all("table", class_="list trackList"):
    #     for j in i.find_all("p", class_="title"):
    #         musicList.append(j.get_text().strip('\n'))

    data_send = {
        'query': text,
        'sessionId': 'a',
        'lang': 'ko',
    }
    data_header = {
        'Authorization': 'Bearer '+ BEARER_TOKEN,
        'Content-Type': 'application/json; charset=utf-8'
    }
    dialogflow_url = 'https://api.dialogflow.com/v1/query?v=20190711'
    res = requests.post(dialogflow_url, data=json.dumps(data_send), headers=data_header)
    if res.status_code != requests.codes.ok:
        return '오류가 발생했습니다.'
    data_receive = res.json()
    answer = data_receive['result']['fulfillment']['speech']
    print(answer)
    return answer
    # return u'\n'.join(answer)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    # block1 = SectionBlock(
    #     text="댕댕"
    # )
    # block2 = SectionBlock(
    #     fields=["text1", "text2", "text3"]
    #     # fields = [block1, block1, ]
    # )

    # block3 = ImageBlock(
    #     image_url="https://boygeniusreport.files.wordpress.com/2016/11/puppy-dog.jpg?quality=98&strip=all",
    #     alt_text="댕댕이"
    # )
    # myblocks = [block1, block2]
    message = _crawl_music_chart(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=message
        # blocks=extract_json(myblocks)
    )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5001)
