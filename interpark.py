# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *

SLACK_TOKEN = "xoxb-521858235120-678258128131-vqxHDLjRVqT6KFSdkFFCgWzk"
SLACK_SIGNING_SECRET = "78c0611b24a09011aa6f29aac4e689b5"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    if not "music" in text:
        return "뮤직?"

    # 여기에 함수를 구현해봅시다.
    url = "http://ticket.interpark.com/Ticket/Goods/GoodsInfo.asp?GoodsCode=19008824"
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, "html.parser")
    musicList = []
    for i in soup.find_all("td", class_="select"):
        print(i)
        # for j in i.find_all("p", class_="title"):
        #     musicList.append(j.get_text().strip('\n'))
    # musicList.append('<https://www.naver.com|네이버로 이동>')
    return u'\n'.join(musicList)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    message = _crawl_music_chart(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=message
        # blocks=extract_json([block1,block2, block3])
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5001)
